import {Socket} from "phoenix"

let socket = new Socket("/socket", {params: {token: window.userToken}});

socket.connect()

const createSocket = (topic_id) => {
  let channel = socket.channel(`comments:${topic_id}`, {});
  channel.join()
    .receive("ok", res => {
      render_comments(res.comments)
    })
    .receive("error", res => { console.log("Unable to join", res) });

    channel.on(`comments:${topic_id}:new`, render_new_comment)

    document.querySelector('button').addEventListener('click', () => {
      const content = document.querySelector('textarea').value;
    
      channel.push('comment:add', { content });
    });
};

function render_comments(comments) {
  const renderedComments = comments.map(comment => {
    return comment_template(comment)
  });
  document.querySelector('.collection').innerHTML = renderedComments.join('');
};

function render_new_comment(event) {
  const rendered_comment = comment_template(event.comment);

  document.querySelector('.collection').innerHTML += rendered_comment;
}

function comment_template(comment) {
  return `<li class="collection-item"> ${comment.content} </li>`
}

window.createSocket = createSocket;
